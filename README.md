<h1>Plugin "Importador de proveedores"</h1>

<strong>Ubicación del plugin:</strong> facturascripts/plugins/importador_proveedores

<br>
Requiere tener activado el plugin de articulos_plus
<br>
<strong>Descripción</strong>

Importacion de csv de proveedor
<br>

<strong>Características:</strong>

<ul>
   <li>Importa csv de proveedores: Supercomp,Infoworks, proximamente depau.</li>
   <li>Compara las referencias y asigna el precio de coste de cada proveedor.</li>
</ul>

<br>

<strong>Funcionamiento interno (Para desarrolladores):</strong>

<ul>
   <li>Rellena el actualiza la instancia del proveedor que se importa</li>
   <li>Actualiza precio con respecto a el precio de coste del proveedor mejor.</li>
   <li>El mejor proveedor esta contemplado como el mas barato que tenga el stock mayor que 0.</li>
   <li>Inicializa a 0 todo el stockaje. (ahora mismo lo hace asi por que no contemplo el stock virtual)<br>
    ademas no controlo el stock en tienda solo quiero saber si el proveedor tiene stock en el momoneto o no<br>
    en proximas versiones se diferenciará entre el stock virtual, que es el que tiene los proveedores y el real en<br>
    tineda</li>
    <li>Al final se actualiza todo el stockaje.</li>
    <li>Se inicializa a 0 por que ya me ha pasado que por ejemplo si la semana pasada un proveedor tiene de stock 4 y luego quita<br>
    el producto del csv a mi aunque haya importado el csv el dia de antes me sigue apareciendo 4.<br>
    ademas como el stock esta a 0 no se puede vender y como yo ya lo actualizo nada y mi lo salto</li>
</ul>


<strong>Sugerencias en consideración:</strong>
 
<ul>
   <li>Por rellenar</li>
</ul>

<br>

<strong>Apoya el plugin:</strong>

Puedes apoyar el desarrollo del plugin sugiriendo y enviando cambios.
