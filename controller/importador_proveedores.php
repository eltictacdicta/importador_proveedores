<?php

require_model('cliente.php');
require_model('familia.php');
require_model('direccion_cliente.php');
require_model('articulo.php');
require_model('articulo_plus.php');
require_model('articulo_proveedor_plus.php');
require_model('impuesto.php');
require_model('almacen.php');
require_model('divisa.php');
require_model('familia.php');
require_model('forma_pago.php');
require_model('impuesto.php');
require_model('serie.php');
require_model('margen_imp_prov.php');
require_model('proveedor.php');
require_model('patron_imp_prov.php');
require_model('cabecera_patron.php');
require_model('redondeo_imp_prov.php');
//require_once('plugins/importador_proveedores/lib/parsecsv.lib.php');
//require_once('plugins/importador_proveedores/lib/csv_to_array.php');
require_once('plugins/importador_proveedores/lib/csvlib.php');

if (!defined('TMP_UPLOAD_PATH')) {
    define('TMP_UPLOAD_PATH', "tmp/" . FS_TMP_NAME);
}

class importador_proveedores extends fs_controller
{
    public $directoriotemp = "./tmp/importador_prove/";
    public $ficherotemp = "temp.csv";
    public $maxsubir = 10; // maximo de admisn de archivo en megas
    public $datosfila;
    public $datosfilaplus;
    public $articulo1;
    public $articuloplus1;
    public $articulo;   
    public $articulo_plus;
    public $articulo_proveedor_plus;
    public $proveedor;
    public $counter;
    public $inicio;
    public $final;
    public $numItems;
    public $inicioactualiza=null;
    public $finalactualiza;
    public $numItemsactualiza;
    public $porcentaje;
    public $data;
    public $filas; // Variable que almacena las filas
    public $dato;
    public $cabecera; // Variable que almacena las cabecera
    public $cabeceras;
    public $traducciones;
    public $cabeceras_archivo;
    public $next = true; // Variable para indicar que hay más items en la paginacion
    public $prev = false; // Variable para indicar que hay items atras
    public $arraymargenes=null;
    public $arrayredondeos=null;
    public $objetoCsv=null;
    public $patron=null;
    public $listado=null;
    public $codproveedor;
    public $columnas;
    public function __construct()
    {
        parent::__construct(__CLASS__, 'Importador de Proveedores', 'Importación', FALSE, TRUE);

    }


    protected function private_core()
    {
        $this->cliente = new cliente();
        $this->cliente_s = FALSE;
        $this->familia = new familia();
        $this->impuesto = new impuesto();
        $this->results = array();
        $this->almacen = new almacen();
        $this->serie = new serie();
        $this->forma_pago = new forma_pago();
        $this->divisa = new divisa();
        $this->patron= new patron_imp_prov();
        $this->cabecera= new cabecera_patron();
        $this->custom_search = TRUE;
        $this->datosfila = new stdClass();//para manejar el articulo de la fila 
        $this->datosfilaplus = new stdClass();//para manejar articulos_plus de la fila
        $this->datosfilaproveedorplus = new stdClass();//para manejar articulos_plus de la fila
        $this->datosfila1 = new stdClass();//para manejar el articulo de la fila 1
        $this->datosfilaplus1 = new stdClass();//para manejar articulos_plus de la fila 1
        $this->articulo = NULL;
        $this->articulo_plus = NULL;
        $this->articulo_proveedor_plus = NULL;
        $this->articulo1 = NULL;
        $this->articuloplus1 = NULL;
        $margenes=new margen_imp_prov();
        $redondeos=new redondeo_imp_prov();
        $this->arraymargenes=$margenes->all();
        $this->arrayredondeos=$redondeos->all();
        $this->codproveedor=NULL;
        $this->columnas=NULL;
        /*if (isset($_GET['proveedor'])) 
            $this->proveedor = $_GET['proveedor'];
        else {
            $this->crear_proveedores();//Solo los crea si no existen
        }*/
        // Comprobamos que el plugin este activo
        if(class_exists("articulo_plus"))
        {
            //new articulo_plus(); // forzamos la creacion de la tabla correspondiente
            if(isset($_GET['codproveedor']))
            {
                $this->codproveedor=$_GET['codproveedor'];
            }
            else 
            {
               if(isset($_POST['codproveedor']))
                {
                    $this->codproveedor=$_POST['codproveedor'];
                } 
            }
            if($this->codproveedor)
            {
                if($this->carga_patron())
                {
                    $this->objetoCsv = new csvLib($this->directoriotemp, $this->ficherotemp, $this->patron->nom_extension($this->patron->extension), 50);
                    if($this->objetoCsv)
                    {
                        if (isset($_GET['nueva_importacion'])) 
                        {
                            if(isset($_FILES['archivo']))
                            {
                                $this->procesaarchivo($_FILES['archivo']);
                            }
                            else 
                            {
                                $this->new_error_msg("No se ha recibido archivo");
                            }
                        }
                        elseif (isset($_GET['importar'])) {
                            $this->importador($this->directoriotemp . $this->ficherotemp);
                        } 
                        else 
                        {
                            //$this->crear_proveedores();
                            $this->template = "nueva_importacion";
                        }
                    }
                    else 
                    {
                        $this->new_error_msg("No se ha podido cargar el objeto csv");
                    }
                }
                else 
                {
                    $this->new_error_msg('Error al cargar el patron o no se ha agregado cabeceras');
                }
            }
            
                
        }
        else{
            $this->new_error_msg("Se requiere el plugin de articulo_plus");
        }

    }

    public function procesaarchivo($archivo)
    {
        $resultado=$this->objetoCsv->subirarchivo($archivo);
        if($resultado===1)
        {
            $this->new_message("La extension y el tamaño es correcto");
            if(file_exists($this->directoriotemp . $this->ficherotemp))
            {
                $this->prev_proveedor();
            }
        }
        else 
        {
            $this->new_error_msg($resultado);
        }
    }

    public function resetear()
    {
        $this->db->exec("SET FOREIGN_KEY_CHECKS = 0");
        $this->db->exec('TRUNCATE TABLE articulos_plus');
        $this->db->exec('TRUNCATE TABLE articulo_proveedor_plus');
        $this->db->exec("SET FOREIGN_KEY_CHECKS = 1");
    }
    
    public function resetearStock()
    {
        $sql = "UPDATE articulos,articulo_proveedor_plus SET articulos.stockfis = 0 WHERE articulos.referencia=articulo_proveedor_plus.referencia AND articulo_proveedor_plus.codproveedor={$this->codproveedor};";
        $this->db->exec($sql);

    }
    
    public function resetearStockProveedor()
    {
        $sql = "UPDATE articulo_proveedor_plus SET stock = 0 WHERE codproveedor={$this->codproveedor};";
        $this->db->exec($sql);
    }

    public function resetear_articulos()
    {
        $this->db->exec("SET FOREIGN_KEY_CHECKS = 0");;
        $this->db->exec('TRUNCATE TABLE articulos');
        $this->db->exec("SET FOREIGN_KEY_CHECKS = 1");
        $this->template = "importador_proveedores";
    }
    
    
    function prev_proveedor()
    {
        $error=false;
        $cabeceras_archivo=$this->objetoCsv->devuelveCabeceras($this->directoriotemp . $this->ficherotemp,$this->patron->separador,false);
        foreach ($this->cabeceras as $cabecera)
        {
            if($cabeceras_archivo[$cabecera->posicion]!==$cabecera->cabecera)
            {
                $error=TRUE;
            }
        }
        if($error)
        {
            $this->new_error_msg('No coinciden las cabeceras con el patron');
        }
        else 
        {
            $this->new_message('Las cabeceras coinciden perfectamente');
            $this->listado = $this->objetoCsv->parseCsv($this->directoriotemp . $this->ficherotemp,$this->patron->separador,1,6,$this->patron->nom_codificacion($this->patron->codificacion),$this->columnas);
            $this->template = 'comprueba_importacion';
        }
        
        
        

    }
    

    function importador($ruta)
    {
        if(!isset($_GET['finalactualiza']))//si existe es que esta acutalizando stock
        {
            $cont=0;
            $trozos=100;
            $inicio;
            $final;
            $numItems;
            $incremento=100;
            $stock=0;

           if(!isset($_GET['final']))
            {
                $this->inicio=0;
                $this->resetearStockProveedor();
                $this->resetearStock();
                $this->numItems=$this->objetoCsv->countCSV($ruta);
                if($this->numItems<100)
                {
                    $incremento=1;
                }
                else
                {
                    //si es mayor de 100 el incremento es de 100 en 100
                    $incremento=100;
                }
                $this->final=$this->inicio+$incremento;
                $this->porcentaje=floor((($this->inicio)*100)/$this->numItems);

            }
            else
            {
                if(isset($_GET['final'])&&isset($_GET['numItems']))
                {
                    $this->inicio=$_GET['final'];
                    $this->numItems=$_GET['numItems'];
                    if($this->numItems<100)
                    {
                        $incremento=1;
                    }
                    else
                    {
                        $incremento=100;
                    }
                    $this->final=$this->inicio+$incremento;
                    $this->porcentaje=floor((($this->inicio)*100)/$this->numItems);
                }


            }
            
            $data = $this->objetoCsv->parseCsv($ruta,$this->patron->separador,$this->inicio,$this->final,$this->patron->nom_codificacion($this->patron->codificacion),$this->columnas);
            $this->new_message("Porcentaje:".$this->porcentaje."% num items ".$this->numItems." inicio ".$this->inicio);
            $counter = 0;
            $referencia='';
            $comienzo=$this->microtime_float();
            $this->datosfilaproveedorplus=new articulo_proveedor_plus();
            $this->datosfila = new articulo();
            $this->datosfilaplus=new articulo_plus();
            foreach ($data as $key => $row) {
                $error = FALSE;
                $exite = FALSE;
                //$this->new_message($this->traducciones['referencia']);
                $referencia=$row[$this->traducciones['referencia']];
                $stock=$row[$this->traducciones['stock']];
                //if(!ctype_digit ( $referencia )&&$referencia=="BX80646I54460")//por ahora las referencias solo numericas no las voy a meter
                //if(!ctype_digit ( $referencia )&&$referencia=="0263B002")//por ahora las referencias solo numericas no las voy a meter
                if(!ctype_digit ( $referencia ) && $stock>0)
                {
                    //$this->new_message("No son solo digitos");

                    $this->datosfilaproveedorplus=$this->datosfilaproveedorplus->get($referencia."_".$this->codproveedor);
                    
                    if(!$this->datosfilaproveedorplus)
                    {
                        $this->datosfilaproveedorplus=$this->rellenaArticuloProveedorPlus( $row);

                    }
                    else
                    {
                        $this->actualizaArticuloProveedorPlus($row);
                    }

                    if (!$this->datosfilaproveedorplus->save())//salva
                    {
                        $this->new_error_msg("No se han podido introducir datos");
                    }


                    $this->datosfila=$this->datosfila->get($referencia);
                    if(!$this->datosfila)
                    {
                        $this->datosfila = new articulo();
                        $this->rellenaArticulo($row);
                        if (!$this->datosfila->save())//salva
                        {
                           $error=TRUE;
                        }
                    }

                    $this->datosfilaplus=$this->datosfilaplus->get($referencia);
                    if(!$this->datosfilaplus)
                    {
                        $this->datosfilaplus=new articulo_plus();
                        $this->rellenaArticuloPlus( $row);
                        if (!$this->datosfilaplus->save())//salva
                        {
                          $this->new_error_msg("No se ha podido agregar articulo Plus");
                          $error=TRUE;
                        }
                    }

                    $this->actualiza($this->datosfila);
                    //$this->actualizaStock($this->datosfila);
                    if (!$this->datosfila->save())//salva
                    {
                        $this->new_error_msg("Error al actualizar");
                    }
                    $counter ++;

                }
            }


            //$this->new_message("Se han importado $counter items");
            if($this->inicio<$this->numItems)
            {
                $fin=$this->microtime_float();
                $this->new_message("Tiempo de ejecución de la función ".($fin-$comienzo));
                $this->template='resultado';
                
                //$this->template=false;
            }
            else
            {
                //$this->template='resultadofinal';
                $this->actualizaStock();

            }
        }
        else
        {
            $this->actualizaStock();

        }
        return true;
    }
    
    public function carga_patron()
    {
        $this->patron=$this->patron->get_by_proveedor($this->codproveedor);
        $this->cabecera=new cabecera_patron();
        $this->cabeceras=$this->cabecera->all_by_patron($this->patron->id);
        $this->traducciones=$this->cabecera->carga_traducciones($this->cabeceras);
        $this->columnas=array();
        foreach ($this->cabeceras as $cabecera)
        {
            $this->columnas[]=$cabecera->posicion;
        }
        if($this->columnas)
            return true;
        else {
            return false;
        }

        
    }


    function truncateFloat($number, $digitos)
    {
        $raiz = 10;
        $multiplicador = pow($raiz, $digitos);
        $resultado = ((int)($number * $multiplicador)) / $multiplicador;
        return number_format($resultado, $digitos);

    }

    /**
     * Elimina los caracteres que producen problemas para la referencias de articulos:
     * Debe tener entre 1 y 25 caracteres. Se admiten letras (excepto �), n�meros, '_', '.', '*', '/' � '-', cualquier otro
     * @param $text string
     * @return string
     */
    public function parseReferencia($text)
    {
        //return preg_replace("/[^0-9a-zA-Z_.*\-]/", "_", $text);
        $text;

    }

    public function anterior_url()
    {
        if($this->prev)
        {
            $offset = $_GET['offset'] - FS_ITEM_LIMIT;
            $offset = ($offset > 0)?$offset:0;
            return "?page=importador_proveedores&proveedor=" . $_GET['proveedor'] . "&visualizar&offset=" . ($offset);
        }
    }
    public function siguiente_url()
    {
        if($this->next)
        {
            return "?page=importador_proveedores&proveedor=" . $_GET['proveedor'] . "&visualizar&offset=" . ($_GET['offset'] + FS_ITEM_LIMIT);
        }
    }
    
    public function rellenaArticulo($row)
    {
        $this->datosfila->descripcion=$row[$this->traducciones['descripcion']];
        $this->datosfila->codproveedor=$this->codproveedor;
        //$this->datosfila->codfamilia = 'INF';
        $this->datosfila->controlstock = 1;
        $this->datosfila->secompra = 1;
        $this->datosfila->sevende = 1;
        $this->datosfila->set_impuesto('IVA21');
        //$articulo->stockfis = $row['STOCK']; // STOCK POR AHORA NO VOY A INCREMENTAR EL VALOR DE SUPERCOM
        $this->datosfila->stockfis = 0;
        //$articulo->equivalencia=$row["Ean13"];
        $this->datosfila->referencia=$row[$this->traducciones['referencia']];
        //$pvp_iva=str_replace(",", ".", $row["Precios con IVA"]);
        $this->datosfila->observaciones = "";
            
                   
            
            

        //$this->new_message('Referencia: '.$articulo->referencia);
    }

    public function pvd_iva($pvd,$iva)
    {
      if($iva!==FALSE)
        if($iva===0)
            return $pvd;
        elseif($iva>0)
            return round($pvd+($pvd*$iva/100), 3);
      else
        return false;
    }
    
    public function actualiza($articulo)
    {

        $pvp;
        $mejorpvd=$this->datosfilaproveedorplus->getmejorpvd($articulo->referencia);
        if($mejorpvd !== FALSE && $articulo)
        {
            foreach ($this->arraymargenes as $margen)
            {
                if($mejorpvd<$margen->coste)
                {
                    $pvp=$this->get_pvp_iva($this->pvd_iva($mejorpvd,$articulo->get_iva()), $margen->margen);
                    break;
                }
            }
            $pvpantes=$pvp;
            $decimalpvp=($pvp-intval($pvp))*100;
            $this->new_message("Decimal de pvp ". $decimalpvp);
            foreach ($this->arrayredondeos as $redondeo)
            {
                
                if($decimalpvp < $redondeo->decimales)
                {
                    $pvp=intval($pvp)+ $redondeo->redondeo;
                    break;
                }
            }
            $this->new_message("El pvp de antes era ". $pvpantes . " y el de ahora ".$pvp);
            
            $this->new_message("Nuevo pvp de es ".$pvp." el mejor pvd es".$mejorpvd);
            if($mejorpvd!==false)
            {
                $articulo->set_pvp_iva($pvp);
            }
            return $articulo;
        }
        else 
        {
            $this->new_error_msg("Error o no se ha importador el archivo o no hay mejor pvd");
            return false;
        }
    }

    public function total_articulos()
   {
      $data = $this->db->select("SELECT COUNT(articulos.referencia) as total FROM articulos,articulo_proveedor_plus WHERE articulos.referencia=articulo_proveedor_plus.referencia AND articulo_proveedor_plus.codproveedor=".$this->codproveedor);
      if($data)
      {
         return intval($data[0]['total']);
      }
      else
         return 0;
   }

    public function actualizaStock()
    {
        $comienzo=$this->microtime_float();
        $arrayArticulos= array();
        $articulo_proveedor_plus=new articulo_proveedor_plus();
        $articulo= new articulo();

        if(!isset($_GET['numItemsactualiza']))
        {
            $this->numItemsactualiza=$this->total_articulos();
            if($this->numItemsactualiza<100)
            {
                $incremento=1;
            }
            else
            {
                $incremento=100;
            }
        }
        else
        {
            $this->numItemsactualiza=$_GET['numItemsactualiza'];
            if($this->numItemsactualiza<100)
            {
                $incremento=1;
            }
            else
            {
                $incremento=100;
            }
        }
        if(!isset($_GET['finalactualiza']))
        {
            $this->inicioactualiza=0;
            
            
            $this->new_message("Longitud del array:".$this->numItemsactualiza);
            
            $this->finalactualiza=$this->inicioactualiza+$incremento;
            $this->porcentaje=floor((($this->inicioactualiza)*100)/$this->numItemsactualiza);

        }
        else
        {
            if(isset($_GET['finalactualiza'])&&isset($_GET['numItemsactualiza']))
            {
                $this->inicioactualiza=$_GET['finalactualiza'];
                
                /*if($incremento==0)
                {
                  $incremento=1;  
                }*/
                $this->finalactualiza=$this->inicioactualiza+$incremento;
                $this->porcentaje=floor((($this->inicioactualiza)*100)/$this->numItemsactualiza);
            }
        }
        $this->new_message("Actualizando precios porcentaje:".$this->porcentaje."% num items ".$this->numItemsactualiza." inicio ".$this->inicioactualiza);
        $arrayArticulos=$this->all_by_plus($this->inicioactualiza,$incremento);
        $this->new_message("Longitud del array:".count($arrayArticulos));
        foreach ($arrayArticulos as $articulo) {
            $stock=$articulo_proveedor_plus->getmejorstock($articulo->referencia);
            if($stock!==FALSE && $stock>0)
            {
                $articulo=$this->actualizaStockArticulo($articulo,$stock);
                
            }
        }
        $fin=$this->microtime_float();
        $this->new_message("Tiempo de ejecución de la función ".($fin-$comienzo));
        if($this->inicioactualiza<$this->numItemsactualiza)
        {
            //$fin=$this->microtime_float();
            //$this->new_message("Tiempo de ejecución de la función ".($fin-$comienzo));
            $this->new_message("Entra en resultasdos");
            $this->template='resultadoactualiza';
            //$this->template='resultadofinal';
            //$this->template=false;
        }
        else
        {
            $this->new_message("Entra en resultasdofinal");
            //$fin=$this->microtime_float();
            //$this->new_message("Tiempo de ejecución de la función ".($fin-$comienzo));
            $this->template='resultadofinalactualiza';
        }

    }

    public function actualizaStockArticulo($articulo,$stock)
    {
        if($articulo->referencia)
        {
            $this->db->exec("UPDATE articulos SET stockfis={$stock} WHERE referencia={$articulo->var2str($articulo->referencia)}");
            
        }
        else 
        {
            return FALSE;
        }
    }
    
    public function get_pvp_iva($pvd_iva,$porcentaje)
    {
        $pvp=$pvd_iva+(($pvd_iva*$porcentaje)/100);
        return $pvp;
    }

    public function rellenaArticuloPlus($row)
    {
        $this->datosfilaplus->referencia = $row[$this->traducciones['referencia']];
        if(!empty($row[$this->traducciones['descripcion_larga']]))
            $this->datosfilaplus->descripcion_larga = $row[$this->traducciones['descripcion_larga']];
        if(!empty($row[$this->traducciones['url_imagen']]))
            $this->datosfilaplus->url_imagen = $row[$this->traducciones['url_imagen']];
        if(!empty($row[$this->traducciones['id_web']]))
            $this->datosfilaplus->id_web = $row[$this->traducciones['id_web']];
        //$this->datosfilaplus->cat_web =$row[$this->traducciones['cat_web']] ;

    }
    
    public function rellenaArticuloProveedorPlus($row)
    {
        $articulo_proveedor_plus=new articulo_proveedor_plus();
        $articulo_proveedor_plus->referenciaplus = $row[$this->traducciones['referencia']]."_".$this->codproveedor;
        $articulo_proveedor_plus->referencia = $row[$this->traducciones['referencia']];
        $articulo_proveedor_plus->codproveedor = $this->codproveedor;
        $pvd=str_replace(",", ".", $row[$this->traducciones['pvd']]);
        if($this->patron->recargo>0)
            $pvd=$pvd+(($pvd*$this->patron->recargo)/100);
        $articulo_proveedor_plus->pvd  = $pvd ;
        $articulo_proveedor_plus->stock = $row[$this->traducciones['stock']];
        return $articulo_proveedor_plus;
    }
    
    public function actualizaArticuloProveedorPlus($row)
    {
        $pvd=str_replace(",", ".", $row[$this->traducciones['pvd']]);
        if($this->patron->recargo>0)
            $pvd=$pvd+(($pvd*$this->patron->recargo)/100);
        $this->datosfilaproveedorplus->pvd = $pvd ;
        $this->datosfilaproveedorplus->stock = $row[$this->traducciones['stock']];
    }
    
    public function prov_in()//Lista los proveedores que coinciden con loa patrones
    {
      $provelist = array();
      $sql= "SELECT * FROM proveedores WHERE codproveedor IN (SELECT codproveedor FROM patrones_imp_prov) ORDER BY nombre ASC";
     
      
      $data = $this->db->select($sql);
      if($data)
      {
         foreach($data as $p)
            $provelist[] = new proveedor($p);
      }
      return $provelist;
    }
    
    function microtime_float()
    {
    list($useg, $seg) = explode(" ", microtime());
    return ((float)$useg + (float)$seg);
    }
    
   public function all_by_plus($offset=0, $limit=FS_ITEM_LIMIT)
   {
      $artilist = array();
      $sql = "SELECT articulos.* FROM articulos,articulo_proveedor_plus WHERE articulos.referencia=articulo_proveedor_plus.referencia AND articulo_proveedor_plus.codproveedor=".$this->codproveedor;
      
      $data = $this->db->select_limit($sql, $limit, $offset);
      if($data)
      {
         foreach($data as $d)
            $artilist[] = new articulo($d);
      }
      
      return $artilist;
   }

    



    
    
    

}
