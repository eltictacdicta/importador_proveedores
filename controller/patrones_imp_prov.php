<?php

/*
 * This file is part of FacturaSctipts
 * Copyright (C) 2015   Francisco Javier Trujillo Jimenez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
require_model('patron_imp_prov.php');
require_model('cabecera_patron.php');
require_model('proveedor.php');
require_once('plugins/importador_proveedores/lib/csvlib.php');

/**
 * Description of opciones_servicios
 *
 * @author javier
 */
class patrones_imp_prov extends fs_controller {

    public $directoriotemp = "./tmp/importador_prove/";
    public $ficherotemp = "patron.csv";
    public $patron;
    public $cabecera;
    public $proveedor;
    public $cabecera_archivo;
    public $listado;
    public $objetoCsv=null;
    public $aux=0;

    public function __construct() {
        parent::__construct(__CLASS__, 'Patrones', 'Importador', FALSE, FALSE);
    }

    protected function private_core() {
        $this->patron = new patron_imp_prov();
        $this->cabecera = new cabecera_patron();
        $this->proveedor = new proveedor();
        
        if (isset($_GET['delete_patron'])) {
            $patron = $this->patron->get($_GET['delete_patron']);
            if ($patron) {
                if ($patron->delete()) {
                    $this->new_message('Patron eliminado correctamente.');
                    if($this->cabecera->delete_by_patron($_GET['delete_patron']))
                    {
                        $this->new_message('Las cabeceras del patron se han eliminado correctamente');
                    }
                    else 
                    {
                        $this->new_error_msg('No existian cabeceras para este patron o no se han podido eliminar');
                    }
                } else
                    $this->new_error_msg('Error al eliminar el margen.');
            } else
                $this->new_error_msg('patron no encontrado.');
        }
        else if (isset($_GET['nuevo_patron'])) {
            if (isset($_FILES['archivo'])) {//si se ha añadido el archivo se carga la otra vista
                if($_POST["separador"]!=="")
                {
                    $ext="csv";
                    if($_POST["extension"]==2)
                    {
                        $ext="txt";
                    }
                    
                    $this->objetoCsv = new csvLib($this->directoriotemp, $this->ficherotemp, $ext, 15);
                    $this->procesaarchivo($_FILES['archivo']);
                    $this->patron->separador=$_POST['separador'];
                    $this->patron->codificacion=$_POST['codificacion'];
                    $this->patron->recargo=$_POST['recargo'];
                    $this->patron->extension=$_POST['extension'];
                    $this->patron->codproveedor=$_POST['codproveedor'];
                    $this->template = "comprueba_patron";
                    $this->cabecera_archivo = $this->objetoCsv->devuelveCabeceras($this->directoriotemp . $this->ficherotemp, $_POST['separador']);
                    $this->listado = $this->objetoCsv->parseCsv($this->directoriotemp . $this->ficherotemp,$_POST['separador'],1,6,$this->patron->nom_codificacion($_POST['codificacion']));
    
                }
                
            }
            else 
            {
                $this->template="nuevo_patron";
            }
            
        } elseif (isset($_GET['agrega_patron'])) {
                $ext="csv";
                if($_POST["extension"]==2)
                {
                    $ext="txt";
                }
                $posicion=0;
                $this->objetoCsv = new csvLib($this->directoriotemp, $this->ficherotemp, $ext, 50);
                $this->cabecera_archivo = $this->objetoCsv->devuelveCabeceras($this->directoriotemp . $this->ficherotemp,$_POST['separador'],false);
                //comprobamos si hay algun registro duplicado hago 2 arrays
                $camposrecogidos=array();
                $camposrequeridos=0;
                foreach ($this->cabecera_archivo as $cabecera)
                {
                    if($_POST['cabecera_'.$posicion]!=='')
                    {
                        $camposrecogidos[]=$_POST['cabecera_'.$posicion];
                    }
                    $posicion++;
                }
                if((count($camposrecogidos))===(count(array_unique($camposrecogidos))))
                {
                    $requeridos=0;
                    foreach ($camposrecogidos as $camporecogido)
                    {
                        if($camporecogido=='referencia')
                        {
                            $requeridos++;
                        }
                        if($camporecogido=='stock')
                        {
                            $requeridos++;
                        }
                        if($camporecogido=='pvd')
                        {
                            $requeridos++;
                        }
                        if($camporecogido=='descripcion')
                        {
                            $requeridos++;
                        }
                    }
                    if($requeridos==4)
                    {
                    
                    $this->patron->separador=$_POST['separador'];
                    $this->patron->codificacion=$_POST['codificacion'];
                    $this->patron->recargo=$_POST['recargo'];
                    $this->patron->extension=$_POST['extension'];
                    $this->patron->codproveedor=$_POST['codproveedor'];
                    
                    $this->patron->id=$this->patron->get_new_codigo();
                    $this->new_message("Separador:".$this->patron->separador." codificacion=".$this->patron->codificacion." recargo=".$this->patron->recargo);
                    if($this->patron->save())
                    {
                        $this->new_message("Patron Guardado correctamente");
                        $posicion=0;
                        foreach ($this->cabecera_archivo as $cabecera)
                        {
                            $this->new_message("Cabecera: ".$cabecera);
                            if(isset($_POST['cabecera_'.$posicion]))
                            {
                                if($_POST['cabecera_'.$posicion]!=='')
                                {
                                    $this->new_message("Cabecera: ".$this->cabecera_archivo[$posicion]);
                                    $this->cabecera->id=$this->cabecera->get_new_codigo();
                                    $this->cabecera->patron=$this->patron->id;
                                    $this->cabecera->cabecera=$cabecera;
                                    $this->cabecera->campo=$_POST['cabecera_'.$posicion];
                                    $this->cabecera->posicion=$posicion;
                                    if($this->cabecera->save())
                                    {
                                        $this->new_message("Cabezera añadida correctamente");
                                    }
                                    else 
                                    {
                                        $this->new_error_msg("Error al añadir la cabecera.");
                                    }
                                }
                            }
                            $posicion++;
                        }

                    }
                    else 
                    {
                        $this->new_error_msg("Error al agregar el patron.");
                    }

                }
                else 
                {
                    $this->new_error_msg("No se ha seleccionado los campos requeridos PVP,Descripcion Corta,Stock o referencia");
                }
            }
            else 
            {
                $this->new_error_msg("Ha repetido registros.");
            }
        }
                       
    }

    public function listar_codificaciones() {


        $codificaciones = array();

        /**
         * En registro_sat::estados() nos devuelve un array con todos los codificaciones,
         * pero como queremos también el id, pues hay que hacer este bucle para sacarlos.
         */
        foreach ($this->patron->codificaciones() as $i => $value)
            $codificaciones[] = array('id_codificacion' => $i, 'nombre_codificacion' => $value);

        return $codificaciones;
    }
    
    public function listar_extensiones() {


        $codificaciones = array();

        /**
         * En registro_sat::estados() nos devuelve un array con todos los codificaciones,
         * pero como queremos también el id, pues hay que hacer este bucle para sacarlos.
         */
        foreach ($this->patron->extensiones() as $i => $value)
            $extensiones[] = array('id_extension' => $i, 'nombre_extension' => $value);

        return $extensiones;
    }
    
    public function prov_not_in()//Lista los proveedores que no coinciden con loa patrones
    {
      $provelist = array();
      $sql= "SELECT * FROM proveedores WHERE codproveedor NOT IN (SELECT codproveedor FROM patrones_imp_prov) ORDER BY nombre ASC";
     
      
      $data = $this->db->select($sql);
      if($data)
      {
         foreach($data as $p)
            $provelist[] = new proveedor($p);
      }
      return $provelist;
    }


    
    
    
    
    


    public function listar_campos() {


        $campos = array();

        /**
         * En registro_sat::estados() nos devuelve un array con todos los codificaciones,
         * pero como queremos también el id, pues hay que hacer este bucle para sacarlos.
         */
        foreach ($this->patron->campos() as $i => $value)
            $campos[] = array('id_campo' => $i, 'nombre_campo' => $value);

        return $campos;
    }
    
    public function procesaarchivo($archivo)
    {
        $resultado=$this->objetoCsv->subirarchivo($archivo);
        if($resultado===1)
        {
            $this->new_message("La extension y el tamaño es correcto");
        }
        else 
        {
            $this->new_error_msg($resultado);
        }
    }
    
    
    
    

    

}
