<?php

/*
 * This file is part of FacturaSctipts
 * Copyright (C) 2015   Francisco Javier Trujillo Jimenez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
require_model('redondeo_imp_prov.php');
/**
 * Description of redondeos_servicios
 *
 * @author javier
 */
class redondeos extends fs_controller
{
   public $redondeo;
   
   public function __construct()
   {
      parent::__construct(__CLASS__, 'redondeos', 'Importador', FALSE, FALSE);
   }
   
   protected function private_core()
   {
      $this->redondeo = new redondeo_imp_prov();
      if( isset($_GET['delete_redondeo']) )
      {
         $redondeo = $this->redondeo->get($_GET['delete_redondeo']);
         if($redondeo)
         {
            if( $redondeo->delete() )
            {
               $this->new_message('Estado eliminado correctamente.');
            }
            else
               $this->new_error_msg('Error al eliminar el redondeo.');
         }
         else
            $this->new_error_msg('Estado no encontrado.');
      }
      else if( isset($_POST['decimales']) )
      {
         $redondeo = $this->redondeo->get($_POST['decimales']);
         if(!$redondeo)
         {
            $redondeo = new redondeo_imp_prov();
            $redondeo->decimales = intval($_POST['decimales']);
         }
         $redondeo->redondeo = $_POST['redondeo'];
         
         
         if( $redondeo->save() )
         {
            $this->new_message('Estado guardado correctamente.');
         }
         else
            $this->new_error_msg('Error al guardar el redondeo.');
      }
   }
}
