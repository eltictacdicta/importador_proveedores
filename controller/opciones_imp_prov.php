<?php

/*
 * This file is part of FacturaSctipts
 * Copyright (C) 2015   Francisco Javier Trujillo Jimenez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
require_model('margen_imp_prov.php');
/**
 * Description of opciones_servicios
 *
 * @author javier
 */
class opciones_imp_prov extends fs_controller
{
   public $margenes;
   
   public function __construct()
   {
      parent::__construct(__CLASS__, 'Opciones', 'Importador', FALSE, FALSE);
   }
   
   protected function private_core()
   {
      $this->margen = new margen_imp_prov();
      if( isset($_GET['delete_margen']) )
      {
         $margen = $this->margen->get($_GET['delete_margen']);
         if($margen)
         {
            if( $margen->delete() )
            {
               $this->new_message('Estado eliminado correctamente.');
            }
            else
               $this->new_error_msg('Error al eliminar el margen.');
         }
         else
            $this->new_error_msg('Estado no encontrado.');
      }
      else if( isset($_POST['coste']) )
      {
         $margen = $this->margen->get($_POST['coste']);
         if(!$margen)
         {
            $margen = new margen_imp_prov();
            $margen->coste = intval($_POST['coste']);
         }
         $margen->margen = $_POST['margen'];
         
         
         if( $margen->save() )
         {
            $this->new_message('Estado guardado correctamente.');
         }
         else
            $this->new_error_msg('Error al guardar el margen.');
      }
   }
}
