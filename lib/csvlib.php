<?php

class csvLib
{
    public $archivo;
    public $ruta;
    public $extension;
    public $limite;
    public $nombArchivo;
    public function __construct ($ruta,$nomArchivo,$extension,$limite) {
        $this->ruta=$ruta;
        $this->nombArchivo=$nomArchivo;
        $this->extension=$extension;
        $this->limite=$limite;

    }
    
    public function countCSV($file, $delimiter = ";")
    {
        if($delimiter=='tab')
                $delimiter=chr(9);
        $count = 0;
        $error=false;
        $handle = fopen($file,'r');
        if($header = fgetcsv($handle, null, $delimiter)) {
            
            while ((($data = fgetcsv($handle, null, $delimiter)) !== FALSE)&& $error===false) {
                $count++;
            }
        }
        fclose($handle);
        return $count;
    }
    
    public function parseCsv($file, $delimiter = ";",$inicio,$fin,$codificacion,$columnas=false) //devuelve un array de filas del csv siempre que coincida con la cabecera, si no devuelve null
    {
        if($delimiter=='tab')
                $delimiter=chr(9);
        $buffer = array();
        $count = 0;
        $error=false;
        $handle = fopen($file,'r');
        if($header = $this->devuelveCabeceras($file, $delimiter,$columnas)) {
            //fgetcsv($handle, null, $delimiter);
            while ((($data = fgetcsv($handle, null, $delimiter)) !== FALSE)&& $error===false) {
                $aux=array();
                if($columnas)
                {
                    foreach ($columnas as $columna)
                    {
                        if($codificacion!=='UTF-8' && is_string ( $data[$columna] ))//comprobamos la codificacion
                        {
                            $aux[]=iconv($codificacion, 'UTF-8', $data[$columna]);
                        }
                        else 
                        {
                            $aux[]=$data[$columna];
                        }
                        
                    }
                    $data=$aux;
                }
                else 
                {
                    if($codificacion!=='UTF-8')//comprobamos la codificacion
                    {
                       foreach ($data as $dato)
                        {
                            if(is_string ( $dato ))//comprobamos que sea de tipo string
                                $dato=iconv($codificacion, 'UTF-8', $dato);
                            $aux[]=$dato;
                        } 
                        $data=$aux;
                    }
                }
                //$buffer[] = array_combine($header, $data);
                /*echo '<br/><br/>';
                echo 'Cabecera <br>';
                print_r($header);
                echo '<br>';
                echo 'Datos ';
                echo '<br>';
                print_r($data);
                echo 'Largura de data '.count($data).'Largura de cabecera'.count($header);
                echo '<br> data 19 '.$data[19].'<br>';*/
                if($count>=$inicio&&$count<=$fin)
                {
                    if((count($data)===(count($header)+ 1))&&(count($data)==20))//fixeo para depau
                    {
                        array_pop($data);
                    }
                    if(@array_combine($header,$data)=== FALSE)
                    {
                        $error=TRUE;
                        $buffer= null;
                    }
                    else
                    {
                        $buffer[] = array_combine($header, $data);
                    }
                }
                $count++;
            }
        }
        fclose($handle);
        //print_r($buffer);
        return $buffer;
    }
    
    public function devuelveCabeceras($file, $delimiter = ";",$columnas=false)
    {
        if($delimiter=='tab')
                $delimiter=chr(9);
        $buffer = array();
        $handle = fopen($file,'r');
        $bom = pack('CCC', 0xEF, 0xBB, 0xBF);
        if($header = fgetcsv($handle, null, $delimiter)) {
            if(@$header[14]=='Fecha de creación del producto') // esto es un arreglo para fixear un fallo en los csv de infoworks
            {
                array_pop($header);
            }
            if (strncmp($header[0], $bom, 3) === 0) {//Si por casualidad esta el fichero en utf-8 con BOM le quita el primer caracter
                    $header[0] = substr($header[0], 3);
            }
        }
        if($columnas)
        {
            $aux=array();
            foreach ($columnas as $columna)
            {
                $aux[]=$header[$columna];
            }
            $header=$aux;
        }
        fclose($handle);
        return $header;
    }
    public function subirarchivo($archivo)//retorna 1 si ha ido todo bian o el mensaje en caso de error.
    {
        $return = "";
        $file_name = $archivo['name'];
        $file_size = $archivo['size'];
        $file_tmp = $archivo['tmp_name'];
        @$file_type = $archivo['image']['type'];
        $nomfile = explode('.', $archivo['name']);
        $file_ext = strtolower(array_pop($nomfile));

        if ($file_ext != $this->extension) {
            $return = "Error: la extension del archivo debe de ser:".$this->extension;
        }
        if ($file_size > (($this->limite * 1024) * 1024)) { //aqui lo combierto a bytes
            $return .= ' El archivo no puede execer de ' . $this->limite . 'MB';
        }
        if ($return === "") {
            if (!is_dir($this->ruta)) {
                mkdir($this->ruta, 0777, true);
            }
            if (move_uploaded_file($file_tmp, $this->ruta . $this->nombArchivo)) {
                $return=1;//devuelve 1 si todo ha ido bien.
            } else {
                $return="El archivo no se ha podido subir tiene que haber habido algun error";
            }
        } 
        return $return;
    }

}
