<?php

/*
 * This file is part of FacturaSctipts
 * Copyright (C) 2015  Carlos Garcia Gomez         neorazorx@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of margen_imp_prov
 *
 * @author carlos
 */
require_model('proveedor.php');
class patron_imp_prov extends fs_model
{
   public $id;
   public $codproveedor;
   public $codificacion;
   public $recargo;
   public $separador;
   public $extension;
   public $cabecera;

   public function __construct($e = FALSE)
   {
      parent::__construct('patrones_imp_prov', 'plugins/importador_proveedores/');
      if($e)
      {
         $this->id = $this->intval($e['id']);
         $this->codproveedor = $e['codproveedor'];
         $this->codificacion = $e['codificacion'];
         $this->recargo = $this->intval($e['recargo']);
         $this->separador = $e['separador'];
         $this->extension = $e['extension'];
      }
      else
      {
         $this->id = null;
         $this->codproveedor = null;
         $this->codificancion = null;
         $this->recargo = 0;
         $this->separador = null;
         $this->separador = null;
      }
   }
   
   protected function install()
   {
       
      /*return "INSERT INTO margenes_imp_prov  (coste,margen) VALUES".
              "('1','100')".
              ", ('2','70')".
              ", ('5','50')".
              ", ('10','25')".
              ", ('20','15')".
              ", ('100','10')".
              ", ('500','9')".
              ", ('700','8')".
              ", ('1000','7')".
              ", ('2000','6')".
              ",('5000','5');";*/
   }
   
   public function get($id)
   {
      $data = $this->db->select("SELECT * FROM patrones_imp_prov WHERE id = ".$this->var2str($id).";");
      if($data)
      {
         return new patron_imp_prov($data[0]);
      }
      else
         return FALSE;
   }
   
   public function get_by_proveedor($codproveedor)
   {
      $data = $this->db->select("SELECT * FROM patrones_imp_prov WHERE codproveedor = ".$this->var2str($codproveedor).";");
      if($data)
      {
         return new patron_imp_prov($data[0]);
      }
      else
         return FALSE;
   }
   
   
   public function exists()
   {
      if( is_null($this->id) )
      {
         return FALSE;
      }
      else
         return $this->db->select("SELECT * FROM patrones_imp_prov WHERE id = ".$this->var2str($this->id).";");
   }
   
   public function get_new_codigo()
   {
      $cod = $this->db->select("SELECT MAX(".$this->db->sql_to_int('id').") as cod FROM ".$this->table_name.";");
      if($cod)
      {
         return (1 + intval($cod[0]['cod']));
      }
      else
         return 1;
   }
   
   public function save()
   {
      $this->id = $this->no_html($this->id);
      $this->codproveedor = $this->no_html($this->codproveedor);
      $this->codificacion = $this->no_html($this->codificacion);
      $this->recargo = $this->no_html($this->recargo);
      $this->separador = $this->no_html($this->separador);
      $this->extension = $this->no_html($this->extension);

      
      if( $this->exists() )
      {
         $sql = "UPDATE patrones_imp_prov SET coproveedor = ".$this->var2str($this->codproveedor).
                 ", codificacion = ".$this->var2str($this->codificacion).
                 ", recargo = ".$this->var2str($this->recargo).
                 ", separador = ".$this->var2str($this->separador).
                 ", extension = ".$this->var2str($this->extension).
                 "  WHERE id = ".$this->var2str($this->id).";";
      }
      else
      {
         $sql = "INSERT INTO patrones_imp_prov (id,codproveedor,codificacion,recargo,separador,extension) VALUES ("
                 .$this->var2str($this->id).","
                 .$this->var2str($this->codproveedor).","
                 .$this->var2str($this->codificacion).","
                 .$this->var2str($this->recargo).","
                 .$this->var2str($this->separador).","
                 .$this->var2str($this->extension).");";
      }
      
      return $this->db->exec($sql);
   }
   
   public function delete()
   {
      return $this->db->exec("DELETE FROM patrones_imp_prov WHERE id = ".$this->var2str($this->id).";");
   }
   
   public function all()
   {
      $elist = array();
      
      $data = $this->db->select("SELECT * FROM patrones_imp_prov ORDER BY id ASC;");
      if($data)
      {
         foreach($data as $d)
            $elist[] = new patron_imp_prov($d);
      }
      
      return $elist;
   }
   
   public function codificaciones()
   {
      $codificaciones = array(
          1 => 'UTF-8',
          2 => 'iso-8859-1'
      );
      
      return $codificaciones;
   }
   
   public function extensiones()
   {
      $codificaciones = array(
          1 => 'csv',
          2 => 'txt'
      );
      
      return $codificaciones;
   }
   
   public function campos()
   {
      $campos = array(
          '' => 'Ignorar',
          'referencia' => 'Referencia',
          'url_imagen' => 'URL de la Imagen',
          'id_web' => 'Id web',
          'cat_web' => 'Categoría Web',
          'referencia' => 'Referencia',
          'stock' => 'Stock',
          'pvd' => 'PVD',
          'descripcion' => 'Descripcion Corta',
          'descripcion_larga' => 'Despcripción Larga'
      );
      
      return $campos;
   }
   
   private function carga_proveedores()
    {
       $lista_proveedores=array();
       $proveedores = $this->db->select("SELECT * FROM proveedores ORDER BY nombre ASC;");
         if($proveedores)
         {
            foreach($proveedores as $p)
               $lista_proveedores[$p['codproveedor']] = $p['nombre'];
            return $lista_proveedores;
         }
         else 
         {
             return null;
         }
         
    }
   
   public function nom_codificacion($ref)
    {
        $codificaciones=$this->codificaciones();
        if($codificaciones)
            return $codificaciones[$ref];
        else {
            return null;
        }
    }
    
    public function nom_extension($ref)
    {
        $extensiones=$this->extensiones();
        return $extensiones[$ref];
    }
    
    public function nom_proveedor($ref)
    {
       $lista_proveedores=array();
       $lista_proveedores=$this->carga_proveedores();
       if($ref)
       {
           if($lista_proveedores[$ref])
            return  $lista_proveedores[$ref];
           else 
            return false;
       }
        else 
            return false;
    }
   
}
