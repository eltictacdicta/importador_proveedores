<?php

/*
 * This file is part of FacturaSctipts
 * Copyright (C) 2015  Carlos Garcia Gomez         neorazorx@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of margen_imp_prov
 *
 * @author carlos
 */
class cabecera_patron extends fs_model
{
   public $id;
   public $cabecera;
   public $patron;
   public $campo;
   public $posicion;
   public function __construct($e = FALSE)
   {
      parent::__construct('cabeceras_patron', 'plugins/importador_proveedores/');
      if($e)
      {
         $this->id = $this->intval($e['id']);
         $this->patron = $this->intval($e['patron']);
         $this->cabecera = $e['cabecera'];
         $this->campo = $e['campo'];
         $this->posicion = $this->intval($e['posicion']);
      }
      else
      {
         $this->id = null;
         $this->patron = null;
         $this->cabecera = null;
         $this->campo = null;
         $this->posicion = null;
      }
   }
   
   protected function install()
   {
       
   }
   
   public function get($id)
   {
      $data = $this->db->select("SELECT * FROM cabeceras_patron WHERE id = ".$this->var2str($id).";");
      if($data)
      {
         return new cabecera_patron($data[0]);
      }
      else
         return FALSE;
   }
   
   
   public function exists()
   {
      if( is_null($this->id) )
      {
         return FALSE;
      }
      else
         return $this->db->select("SELECT * FROM cabeceras_patron WHERE id = ".$this->var2str($this->id).";");
   }
   
   public function save()
   {
      $this->patron = $this->no_html($this->patron);
      $this->cabecera = $this->no_html($this->cabecera);
      $this->campo = $this->no_html($this->campo);
      
      if( $this->exists() )
      {
         $sql = "UPDATE cabeceras_patron SET parton = ".$this->var2str($this->patron).
                 ", cabecera = ".$this->var2str($this->cabecera).
                 ", campo = ".$this->var2str($this->campo).
                 ", posicion = ".$this->var2str($this->posicion).
                 "  WHERE id = ".$this->var2str($this->id).";";
      }
      else
      {
         $sql = "INSERT INTO cabeceras_patron (id,patron,cabecera,posicion,campo) VALUES ("
                 .$this->var2str($this->id).","
                 .$this->var2str($this->patron).","
                 .$this->var2str($this->cabecera).","
                 .$this->var2str($this->posicion).","
                 .$this->var2str($this->campo).");";
      }
      
      return $this->db->exec($sql);
   }
   
   public function delete()
   {
      return $this->db->exec("DELETE FROM cabeceras_patron WHERE id = ".$this->var2str($this->id).";");
   }
   
   public function delete_by_patron($patron)
   {
      return $this->db->exec("DELETE FROM cabeceras_patron WHERE patron = ".$this->var2str($patron).";");
   }
   
   public function all()
   {
      $elist = array();
      
      $data = $this->db->select("SELECT * FROM cabeceras_patron ORDER BY id ASC;");
      if($data)
      {
         foreach($data as $d)
            $elist[] = new cabecera_patron($d);
      }
      
      return $elist;
   }
   
   public function all_by_patron($patron)
   {
      $elist = array();
      
      $data = $this->db->select("SELECT * FROM cabeceras_patron WHERE patron=".$this->var2str($patron)." ORDER BY id ASC;");
      if($data)
      {
         foreach($data as $d)
            $elist[] = new cabecera_patron($d);
      }
      
      return $elist;
   }
   
   public function carga_traducciones($cabeceras)
    {
       $traducciones=array();
         if($cabeceras)
         {
            foreach($cabeceras as $cabecera)
               $traducciones[$cabecera->campo] = $cabecera->cabecera;
            return $traducciones;
         }
         else 
         {
             return null;
         }
         
    }
   
   public function get_new_codigo()
   {
      $cod = $this->db->select("SELECT MAX(".$this->db->sql_to_int('id').") as cod FROM ".$this->table_name.";");
      if($cod)
      {
         return (1 + intval($cod[0]['cod']));
      }
      else
         return 1;
   }
}
