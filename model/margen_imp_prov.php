<?php

/*
 * This file is part of FacturaSctipts
 * Copyright (C) 2015  Carlos Garcia Gomez         neorazorx@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of margen_imp_prov
 *
 * @author carlos
 */
class margen_imp_prov extends fs_model
{
   public $coste;
   public $margen;
   
   public function __construct($e = FALSE)
   {
      parent::__construct('margenes_imp_prov', 'plugins/importador_proveedores/');
      if($e)
      {
         $this->coste = $this->intval($e['coste']);
         $this->margen = $this->intval($e['margen']);
      }
      else
      {
         $this->coste = NULL;
         $this->margen = NULL;
      }
   }
   
   protected function install()
   {
       
      return "INSERT INTO margenes_imp_prov  (coste,margen) VALUES".
              "('1','100')".
              ", ('2','70')".
              ", ('5','50')".
              ", ('10','25')".
              ", ('20','15')".
              ", ('100','10')".
              ", ('500','9')".
              ", ('700','8')".
              ", ('1000','7')".
              ", ('2000','6')".
              ",('5000','5');";
   }
   
   public function get($coste)
   {
      $data = $this->db->select("SELECT * FROM margenes_imp_prov WHERE coste = ".$this->var2str($coste).";");
      if($data)
      {
         return new margen_imp_prov($data[0]);
      }
      else
         return FALSE;
   }
   
   
   public function exists()
   {
      if( is_null($this->coste) )
      {
         return FALSE;
      }
      else
         return $this->db->select("SELECT * FROM margenes_imp_prov WHERE coste = ".$this->var2str($this->coste).";");
   }
   
   public function save()
   {
      $this->margen = $this->no_html($this->margen);
      
      if( $this->exists() )
      {
         $sql = "UPDATE margenes_imp_prov SET margen = ".$this->var2str($this->margen).
                 "  WHERE coste = ".$this->var2str($this->coste).";";
      }
      else
      {
         $sql = "INSERT INTO margenes_imp_prov (coste,margen) VALUES ("
                 .$this->var2str($this->coste).","
                 .$this->var2str($this->margen).");";
      }
      
      return $this->db->exec($sql);
   }
   
   public function delete()
   {
      return $this->db->exec("DELETE FROM margenes_imp_prov WHERE coste = ".$this->var2str($this->coste).";");
   }
   
   public function all()
   {
      $elist = array();
      
      $data = $this->db->select("SELECT * FROM margenes_imp_prov ORDER BY coste ASC;");
      if($data)
      {
         foreach($data as $d)
            $elist[] = new margen_imp_prov($d);
      }
      
      return $elist;
   }
}
