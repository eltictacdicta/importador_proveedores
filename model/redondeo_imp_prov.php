<?php

/*
 * This file is part of FacturaSctipts
 * Copyright (C) 2015  Carlos Garcia Gomez         neorazorx@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of redondeo_imp_prov
 *
 * @author carlos
 */
class redondeo_imp_prov extends fs_model
{
   public $decimales;
   public $redondeo;
   
   public function __construct($e = FALSE)
   {
      parent::__construct('redondeos_imp_prov', 'plugins/importador_proveedores/');
      if($e)
      {
         $this->decimales = $this->intval($e['decimales']);
         $this->redondeo = floatval($e['redondeo']);
      }
      else
      {
         $this->decimales = NULL;
         $this->redondeo = NULL;
      }
   }
   
   protected function install()
   {
       
      return "";
   }
   
   public function get($decimales)
   {
      $data = $this->db->select("SELECT * FROM redondeos_imp_prov WHERE decimales = ".$this->var2str($decimales).";");
      if($data)
      {
         return new redondeo_imp_prov($data[0]);
      }
      else
         return FALSE;
   }
   
   
   public function exists()
   {
      if( is_null($this->decimales) )
      {
         return FALSE;
      }
      else
         return $this->db->select("SELECT * FROM redondeos_imp_prov WHERE decimales = ".$this->var2str($this->decimales).";");
   }
   
   public function save()
   {
      $this->redondeo = $this->no_html($this->redondeo);
      
      if( $this->exists() )
      {
         $sql = "UPDATE redondeos_imp_prov SET redondeo = ".$this->var2str($this->redondeo).
                 "  WHERE decimales = ".$this->var2str($this->decimales).";";
      }
      else
      {
         $sql = "INSERT INTO redondeos_imp_prov (decimales,redondeo) VALUES ("
                 .$this->var2str($this->decimales).","
                 .$this->var2str($this->redondeo).");";
      }
      
      return $this->db->exec($sql);
   }
   
   public function delete()
   {
      return $this->db->exec("DELETE FROM redondeos_imp_prov WHERE decimales = ".$this->var2str($this->decimales).";");
   }
   
   public function all()
   {
      $elist = array();
      
      $data = $this->db->select("SELECT * FROM redondeos_imp_prov ORDER BY decimales ASC;");
      if($data)
      {
         foreach($data as $d)
            $elist[] = new redondeo_imp_prov($d);
      }
      
      return $elist;
   }
}
